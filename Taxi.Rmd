---
title: "Analisis descriptivo del uso de taxis"
author: "Ander Elexpuru & Ignacio Saiz"
date: "14/2/2020"
output:
  html_document:
    df_print: paged
    code_folding: hide # show
    highlight: tango
    theme: flatly
    toc: yes
    toc_depth: 3
    toc_float: yes
    number_sections: yes


---


---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(ggplot2)
library(ggmap)
library(prettymapr)
library(RgoogleMaps)
library(maps)
library(zoom )
library(plotly)
library(stringr)
library(data.table)
library(dplyr)
library(plyr)
library(mapdeck)
library(forecast)
library(tseries)
library(TSplotly)
library(lubridate)

```
#Bussines understanding

## Enfoque estrategico
Mexico DF es una de las ciudades mas paobladas del mundo, y en este contexto, podemos intuir que existen problemas de movilidad y para ello se ha decidido realizar un estudio descrptivo de las diferentes empresas de taxis que operan en la ciudad.


## Objetivo

Mejorar el plan de movilidad ciudana detectando puntos calientes de recogida y de destino, para asi poder estimar mejor la demanda de transporte publico o privado en ciertas zonas de la ciudad y poder ofrecer una mejor calidad de servicio, ya sea ofreciendo lineas de tranporte publico nuevas o aumnetando el numero de taxis que operan.

#Data understanding

##Fuentes de datos 

Para llevar a cabo el proyecto se ha utilizado un dataset que contiene datos de los viajes en taxi realizados desde Junio de 2016 hasta Julio de 2017.
(<https://www.kaggle.com/mnavas/taxi-routes-for-mexico-city-and-quito>)

```{r data read, echo=FALSE}
datos= read.csv("data/mex_clean.csv")
summary(datos)
```

##Tratamiento de datos

Se han aplicado las siguientes transformaciones a los datos:

1. Dividir las columnas de fecha de recogida y de dejada ("pickupp_datetime" y "dropoff_datetime") en dos columnas distintas cada una, para separar fecha y hora. 
2. Eliminar las columnas que contienen informacion repetida.
3. Pasar de metros a Kilometros la columna de distancia del viaje ("dis_meters").
4. Pasar de segundos a minutos el tiempo de viaje ("trip_duration").
5. Crear un maestro de emprseas con su nombre completo y acronimos.

```{r ETL, echo=FALSE, warning=FALSE, message=FALSE}
datos= read.csv("data/mex_clean.csv")
#datos=datos[0:1000,]
setDT(datos)[, paste0("pickup_datetime", 1:2) := tstrsplit(datos$pickup_datetime, " ")]  
setDT(datos)[, paste0("dropoff_dattime", 1:2) := tstrsplit(datos$dropoff_datetime, " ")]

datos$pickup_datetime=strsplit(as.character(datos$pickup_datetime),' ')
datos$pickup_datetime1=as.Date(datos$pickup_datetime1)
datos$pickup_datetime=NULL
datos$dist_meters=datos$dist_meters/1000
datos$trip_duration=datos$trip_duration/60
datos$weekday=wday(datos$pickup_datetime1)

revalue(datos$vendor_id, c("México DF UberSUV"="México DF Uber")) -> datos$vendor_id
revalue(datos$vendor_id, c("México DF UberXL"="México DF Uber")) -> datos$vendor_id
revalue(datos$vendor_id, c("México DF UberX"="México DF Uber")) -> datos$vendor_id
revalue(datos$vendor_id, c("México DF UberBlack"="México DF Uber")) -> datos$vendor_id
a <- levels (as.factor(datos$vendor_id))
tm.vendors <- data_frame(vendor_id = a, vendor.acr = c("Radio", "Sitio", "Libre", "Uber"))
datos=datos%>%left_join(., tm.vendors)

summary(datos)
```

#Modeling

##Nº de viajes y Km por empresa

```{r V/E, echo=FALSE, warning=FALSE, message=FALSE}
temporal=data.frame(table(datos$vendor_id))
temporal%>%left_join(., tm.vendors, by=c("Var1"="vendor_id"))%>%
  plot_ly(., y= ~reorder(vendor.acr,Freq), x=~Freq, type="bar", orientation="h" )%>%
  layout(yaxis = list(showgrid = FALSE, showline = FALSE, showticklabels = TRUE, domain=c(0, 0.95)),
       xaxis = list(zeroline = FALSE, showline = FALSE, showticklabels = FALSE, showgrid = FALSE, label=TRUE ))%>%
  layout(title=list( text="<b>Nº viajes por empresa<b>"))%>%
  layout(xaxis= list(title=""), yaxis= list(title=""))%>%
  add_annotations(x = ~Freq-160,  y = ~vendor.acr,
                  text = ~Freq,
                  font = list(family = 'Arial', size = 12, color = 'black'),
                  showarrow = FALSE)

datos$vendor.acr=as.factor(datos$vendor.acr)
datos$vendor_id=as.factor(datos$vendor_id)
datos$dist_meters=as.double(datos$dist_meters)
aggregate(datos$dist_meters, by=list(Category=datos$vendor.acr), FUN=sum)%>%
  plot_ly(., y= ~reorder(Category,x), x=~x, type="bar", orientation="h" )%>%
  layout(yaxis = list(showgrid = FALSE, showline = FALSE, showticklabels = TRUE, domain=c(0, 0.95)),
         xaxis = list(zeroline = FALSE, showline = FALSE, showticklabels = FALSE, showgrid = FALSE, label=TRUE ))%>%
  layout(title=list( text="<b>Nº km por empresa<b>"))%>%
  layout(xaxis= list(title=""), yaxis= list(title=""))%>%
  add_annotations(x = ~x+5000,  y = ~Category,
                  text = ~x,
                  font = list(family = 'Arial', size = 12, color = 'black'),
                  showarrow = FALSE)

temporal=data.frame(table(datos$vendor_id,datos$pickup_datetime1))
temporal$weekday=wday(temporal$Var2)
medias= aggregate(temporal$Freq, by = list(temporal$Var1,temporal$weekday), FUN=mean ) 
colnames(medias) = c('empresa', 'weekday', 'media')
medias$weekday=as.factor(medias$weekday)
medias=reshape(medias,timevar="empresa",idvar="weekday",direction="wide")
colnames(medias) = c('weekday', 'Radio.Taxi', 'Taxi.de.Sitio','Taxi.Libre','Uber')
medias %>%
  plot_ly( y=~weekday, x=~round(Taxi.Libre), type = 'bar', name ='Libre', text = ~round(Taxi.Libre), textposition = 'auto')%>%
  add_trace(x=~round(Taxi.de.Sitio), name='Sitio', text = ~round(Taxi.de.Sitio), textposition = 'auto')%>%
  add_trace(x=~round(Radio.Taxi), name='Radio', text = ~round(Radio.Taxi), textposition = 'auto')%>%
  add_trace(x=~round(Uber), name='Uber', text = ~round(Uber), textposition = 'auto')%>%
  layout(xaxis = list(showgrid = FALSE, showline = FALSE, showticklabels = FALSE, domain=c(0, 0.95)))%>%
  layout(title= list(text="<b>Media de viajes por dia de la semana<b>"), xaxis= list(title=""))


kms= aggregate(datos$dist_meters,by=list(datos$vendor.acr, datos$weekday), FUN=mean)
colnames(kms) = c('empresa', 'weekday', 'media')
kms$weekday=as.factor(kms$weekday)
kms=reshape(kms,timevar="empresa",idvar="weekday",direction="wide")
colnames(kms) = c('weekday', 'Radio.Taxi', 'Taxi.de.Sitio','Taxi.Libre','Uber')
kms %>%
  plot_ly( x=~weekday, y=~round(Taxi.Libre), type = 'scatter', mode='lines', name ='Libre')%>%
  add_trace(y=~round(Taxi.de.Sitio), name='Sitio')%>%
  add_trace(y=~round(Radio.Taxi), name='Radio')%>%
  add_trace(y=~round(Uber), name='Uber')%>%
  layout(yaxis = list(showgrid = TRUE, showline = FALSE, showticklabels = TRUE, autotick=FALSE),
         xaxis = list(zeroline = TRUE, showline = FALSE, showticklabels = TRUE, showgrid = FALSE, label=TRUE, autotick = FALSE ))%>%
  layout(title= list(text="<b>Media de Km por dia de la semana<b>"), yaxis= list(title=""))

```

##Serie temporal

```{r V/E, echo=FALSE, warning=FALSE, message=FALSE}



temporal%>%
    plot_ly(.,x=.$Var2, y=.$Freq,type = 'scatter', mode="lines", color = .$Var1)
```

##Mapas de calor
```{r V/E, echo=FALSE, warning=FALSE, message=FALSE}
    
set_token(Sys.getenv("MAPBOX"))


datos %>%
  plot_mapbox(lat = ~pickup_latitude, lon = ~pickup_longitude,
              split = ~vendor_id, size=2,
              mode = 'scattermapbox', hoverinfo='name') %>%
  layout(title = 'Carreras de taxi Mexico DF',
         font = list(color='white'),
         plot_bgcolor = '#191A1A', paper_bgcolor = '#191A1A',
         mapbox = list(style = 'satellite-streets',
                       zoom=8,
                       center = list(lat = median(datos$pickup_latitude),
                                     lon = median(datos$pickup_longitude))),
         legend = list(orientation = 'h',
                       font = list(size = 8)),
         
         margin = list(l = 25, r = 25,
                       b = 25, t = 25,
                       pad = 2)) %>%
  config(mapboxAccessToken = Sys.getenv("MAPBOX_TOKEN"))

datos %>%
  plot_ly(
    type = 'densitymapbox',
    lat = ~pickup_latitude,
    lon = ~pickup_longitude,
    coloraxis = 'coloraxis',
    radius = 2) %>%
  layout(
    mapbox = list(zoom=11,
      style="stamen-terrain",
      center= list(lat = ~pickup_latitude[1],
                   lon = ~pickup_longitude[1])), coloraxis = list(colorscale = "Viridis"))


datos %>%
  plot_ly(
    type = 'densitymapbox',
    lat = ~dropoff_latitude,
    lon = ~dropoff_longitude,
    coloraxis = 'coloraxis',
    radius = 3) %>%
  layout(
    mapbox = list(zoom=11,
                  style="stamen-terrain",
                  center= list(lat = ~median(dropoff_latitude),
                               lon = ~median(dropoff_longitude))), coloraxis = list(colorscale = "RdBu"))

```

##Modelo ARIMA

```{r V/E, echo=FALSE, warning=FALSE, message=FALSE}
datos%>% plot_ly(., x=~dist_meters, y=~trip_duration, color = ~vendor_id, type = 'scatter')



datos_arima=aggregate(temporal$Freq, by=list(as.Date(temporal$Var2)), FUN=sum)
datos_arima$weekday=cbind(wday(datos_arima$Group.1))
colnames(datos_arima)=c('fecha','x','weekday')

xreg= cbind(weekday=model.matrix(~as.factor(datos_arima$weekday)),
            fecha=as.Date(datos_arima$fecha))
xreg=xreg[,-1]
colnames(xreg)=c('lun','mar','mie','jue','vie','sab','fecha')

seria_arima= aggregate(temporal$Freq, by=list(as.Date(temporal$Var2)), FUN=sum)
ts=ts(datos_arima$x)
modelo_arima = auto.arima(ts, xreg = xreg)
TSplot_gen("all",modelo_arima,xreg)
```